using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using WebAPIApplication.Services;
using System.IO;
using Serilog;

namespace WebAPIApplication.Controllers
{
    [Route("api/[controller]")]
    public class CurrencyController : Controller
    {

        private readonly ICurrencyService _currencyService;

        public CurrencyController(ICurrencyService currencyService)
        {
            _currencyService = currencyService;
        }

        // GET api/currency/remote
        [HttpGet("remote")]
        public async Task<string> GetFromRemote()
        {
            Log.Information("waaaat");
            string responseBody = await _currencyService.GetCurrenciesFromRemote();
            return responseBody;
        }

        // GET api/currency/remote
        [HttpGet("local")]
        public async Task<string> GetFromLocal(HttpRequestMessage request)
        {
            string responseBody = await _currencyService.ReadCurrenciesFromFS();
            return responseBody;
        }

        // GET api/currency/remote
        [HttpGet("sync")]
        public async Task<HttpResponseMessage> Sync(HttpRequestMessage request)
        {
            bool result = await _currencyService.SyncCurrenciesToFS();
            if (result){
                return new HttpResponseMessage(HttpStatusCode.OK);
            } else{ 
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
        }

    }
}
