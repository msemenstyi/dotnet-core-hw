using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

namespace WebAPIApplication.Services
{

	public class CurrencyService: ICurrencyService
	{

		public async Task<bool> SyncCurrenciesToFS(){
			string currencies = await GetCurrenciesFromRemote();
			bool isFileSaved = await SaveCurrenciesToFS(currencies);
			return isFileSaved;
		}
		
		public async Task<string> ReadCurrenciesFromFS(){
			
			string result;

			using (StreamReader reader = File.OpenText("./currencies.json"))
			{
				Console.WriteLine("Opened file.");
				result = await reader.ReadToEndAsync();
			}
			return result;
		}

		public async Task<string> GetCurrenciesFromRemote()
		{
			HttpClient client = new HttpClient();
			HttpResponseMessage response = await client.GetAsync("http://resources.finance.ua/ua/public/currency-cash.json");
			response.EnsureSuccessStatusCode();
			string responseBody = await response.Content.ReadAsStringAsync();
			return responseBody;
		}

		private async Task<bool> SaveCurrenciesToFS(string text){
			
			bool isFileSaved = false;

			using (StreamWriter outputFile = File.CreateText("./currencies.json")) {
				await outputFile.WriteAsync(text);
			}
			isFileSaved = true;
			return isFileSaved;
		}

	}
}
