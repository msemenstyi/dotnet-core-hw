using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebAPIApplication.Services
{
    public interface ICurrencyService
    {
        
        Task<bool> SyncCurrenciesToFS();
        Task<string> ReadCurrenciesFromFS();
        Task<string> GetCurrenciesFromRemote();

    }
}